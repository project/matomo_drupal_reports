(function($) {
	Drupal.behaviors.matomo_drupal_reports = {
		attach: function (context, settings) {
			$('[id^=edit-matomo-start] + div').html('');
			$('[id^=edit-matomo-end] + div').hide();
			$('.form-item-matomo-end-date-date label[for^="edit-matomo-end-date ').hide();
			$('.form-item-matomo-end-date > label[for^="edit-matomo-end-date"]').attr('style',"float: left; margin-top: 3% !important; margin-right: 8px;");
			$( '#edit-matomo-start-date-datepicker-popup-0' ).change(function(){
				$('[id^=edit-matomo-end-date').prop('disabled', true); 
			});
			
			var arr_chk_ids = ['edit-visitors-main-tab', 'edit-visitors-locations-main-tab', 'edit-visitors-devices-main-tab', 'edit-visitors-software-main-tab', 'edit-visitors-times-main-tab', 'edit-behaviour-main-tab',
				'edit-behaviour-site-search-main-tab', 'edit-behaviour-events-main-tab', 'edit-behaviour-engagement-main-tab', 'edit-acquisition-main-tab', 'edit-goals-main-tab', 'edit-seo-main-tab'];

			$.each(arr_chk_ids, function( i, chk_id ) {
				$('#' + chk_id).click(function() {
					var chk = $('input[parent-link="' + $(this).attr('parent') + '"]');
				    if($(this).is(":checked") == true){
				        $.each(chk, function( index, value ) {
				        	$(value).prop('checked', true);
				        	$(value).prop("disabled", false);
			        	});
				    }
				    if($(this).not(':checked').length) {
				    	$.each(chk, function( index, value ) {
				        	$(value).prop('checked', false);
				        	$(value).prop("disabled", true);
			        	});
				    }
				});
        	});
		}
	};
})(jQuery);