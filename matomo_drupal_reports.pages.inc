<?php

/**
 * @file
 * Reports callback for Piwik Reports.
 */

/**
 * Return the $form that handles piwik reports.
 *
 * @param array $form
 *   See form API.
 * @param array $form_state
 *   See form API.
 * @param string $report
 *   The report name.
 *
 * @return array
 *   See form API.
 */
function matomo_reports_reports($form, &$form_state, $report) {

  global $user;

  $matomo_auth = (check_plain(variable_get('matomo_drupal_reports_token_auth', '') ? variable_get('matomo_drupal_reports_token_auth', '') : $user->data['matomo_drupal_reports_token_auth']));
  $matomo_url = matomo_drupal_reports_get_url();
  $sites = matomo_drupal_reports_get_sites($matomo_auth);
  if (empty($sites)) {
    $_SESSION['matomo_reports_site'] == '';
    drupal_set_message(t('You cannot access any data on the selected Matomo server. Please check authentication string and permissions with your Matomo server administrator.'), 'warning');
    return;
  }
  if (isset($sites['result']) && $sites['result'] == 'error') {
    $_SESSION['matomo_reports_site'] == '';
    drupal_set_message(t($sites['message']), 'warning');
  }
  $form['#attributes'] = array(
      'class' => array(
          'search-form',
          'container-inline',
      ),
  );

  $form['#attached']['css'][] = drupal_get_path('module', 'matomo_drupal_reports') . '/css/matomo_drupal_reports.css';
  $form['#attached']['js'][] = drupal_get_path('module', 'matomo_drupal_reports') . '/js/matomo_drupal_reports.js';

  $form['matomo_filters'] = array(
      '#type' => 'fieldset',
      '#title' => t('Select site and time period'),
  );

  $options = array();
  $allowed_keys = explode(',', variable_get('matomo_drupal_reports_allowed_sites', ''));
  $session_site_exists = FALSE;
  foreach ($sites as $site) {
    if (empty($allowed_keys[0]) || in_array($site['idsite'], $allowed_keys)) {
      $options[$site['idsite']] = $site['name'];
    }
    if (isset($_SESSION['matomo_reports_site']) && $_SESSION['matomo_reports_site'] == $site['idsite']) {
      $session_site_exists = TRUE;
    }
  }
  if (!isset($_SESSION['matomo_reports_site']) || $_SESSION['matomo_reports_site'] == '' || !$session_site_exists || !in_array($_SESSION['matomo_reports_site'], $allowed_keys)) {
    // When not set, set to first of the allowed sites.
    $_SESSION['matomo_reports_site'] = $allowed_keys[0];
  }
  if (count($options) > 1) {
    $form['matomo_filters']['site'] = array(
        '#type' => 'select',
        '#title' => t('Site:'),
        '#weight' => -5,
        '#default_value' => $_SESSION['matomo_reports_site'],
        '#options' => $options,
    );

    $form['matomo_filters']['matomo-start-date'] = array(
      '#title' => t('Start Date'),
      '#type' => 'date_popup',
      '#date_format' => 'd/m/Y',
      '#default_value' => isset($_SESSION['matomo_reports_start_date']) ? $_SESSION['matomo_reports_start_date'] : date('Y-m-d', strtotime('-7 days')),
      '#date_year_range' => '-20:+3',
      '#size' => 12,
      '#required' => true,
      '#ajax' => array(
          'callback' => '_ajax_matomo_report_end_date_callback',
          'wrapper' => 'matomo-report-end-date-wrapper',
          'method' => 'replace',
          'effect' => 'fade',
      ),
      '#datepicker_options' => array('maxDate' => date('d/m/Y')),
      '#states' => array(
        'invisible' => array(
          ':input[name="period_or_date"]' => array('value' => 'period'),
        ),
      ),
    );

    $form['matomo_filters']['matomo-end-date'] = array(
      '#title' => t('End Date'),
      '#type' => 'date_popup',
      '#date_format' => 'd/m/Y',
      '#default_value' => isset($_SESSION['matomo_reports_end_date']) ? $_SESSION['matomo_reports_end_date'] : date('Y-m-d'),
      '#date_year_range' => '-20:0',
      '#size' => 12,
      '#required' => true,
      '#prefix' => '<div id="matomo-report-end-date-wrapper">',
      '#suffix' => '</div>',
      '#datepicker_options' => array('minDate' => isset($_SESSION['matomo_reports_start_date']) ? date('d/m/Y', strtotime($_SESSION['matomo_reports_start_date'])) : date('d/m/Y', strtotime('-7 days'))),
      '#states' => array(
        'invisible' => array(
          ':input[name="period_or_date"]' => array('value' => 'period'),
        ),
      ),
    );

    if( arg(1) == 'ajax' ) {
      $form['matomo_filters']['matomo-end-date']['#datepicker_options'] = array('minDate' => date('d/m/Y', strtotime($form_state['values']['matomo-start-date'])));
    }

  }
  elseif (count($options) == 1) {
    foreach ($options as $siteid => $sitename) {
      break;
    }
    $form['matomo_filters']['site'] = array(
        '#type' => 'hidden',
        '#value' => $siteid,
    );
    $form['matomo_filters']['sitename'] = array(
        '#type' => 'textfield',
        '#title' => t('Site:'),
        '#weight' => -5,
        '#size' => 25,
        '#value' => $sitename,
        '#disabled' => TRUE,
    );
    $form['matomo_filters']['period']['#attributes'] = ['onchange' => 'this.form.submit();'];
  }

  $form['matomo_filters']['period_or_date'] = array(
      '#type' => 'select',
      '#title' => 'Period/Range ',
      '#default_value' => isset($_SESSION['matomo_reports_period_or_date']) ? $_SESSION['matomo_reports_period_or_date'] : 'period',
      '#options' => ['period' => 'Period', 'range' => 'Range'],
      '#weight' => -3,
  );

  $period = array(
      0 => t('Today'),
      1 => t('Yesterday'),
      2 => t('Last week'),
      3 => t('Last month'),
      4 => t('Last year'),
  );

  $form['matomo_filters']['period'] = array(
      '#type' => 'select',
      '#title' => 'Period',
      '#default_value' => isset($_SESSION['matomo_reports_period']) ? $_SESSION['matomo_reports_period'] : 0,
      '#options' => $period,
      '#weight' => -1,
      '#states' => array(
          'invisible' => array(
              ':input[name="period_or_date"]' => array('value' => 'range'),
          ),
      ),
  );

  if( $report == 'dashboard' ) {
    unset( $form['matomo_filters']['period'] );
    unset( $form['matomo_filters']['period_or_date'] );
    unset( $form['matomo_filters']['matomo-end-date'] );
    unset( $form['matomo_filters']['matomo-start-date'] );
    $form['matomo_filters']['#title'] = t('Select site');
  }

  if( $report == 'all_websites_dashboard' ) {
    unset( $form['matomo_filters']['period_or_date'] );
    unset( $form['matomo_filters']['matomo-end-date'] );
    unset( $form['matomo_filters']['matomo-start-date'] );

  }

  $form['matomo_filters']['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Filter'),
      '#weight' => 15,
  );

  $matomo_site_id = $_SESSION['matomo_reports_site'];

  $period = isset($_SESSION['matomo_reports_period']) ? $_SESSION['matomo_reports_period'] : 0;

  if( !isset( $form_state['values']['period_or_date'] ) ) {
    if ($period == 1) {
      // Special handling for "yesterday" = 1.
      // The yesterday date value is required.
      $date = _matomo_reports_select_period($period);
    }
    else {
      // Otherwise it returns the today date value.
      $date = _matomo_reports_select_period(0);
    }
    $period_name = _matomo_reports_get_period_name($period);
  } else {
    if( arg(1) == 'ajax' ) {
      $date = date('Y-m-d', strtotime($form_state['values']['matomo-start-date'])) . ',' . date('Y-m-d', strtotime($form_state['values']['matomo-end-date']));
    } else {
      $date = date('Y-m-d') . ',' . date('Y-m-d');
    }
  }

// print_r( $date );
// print_r( $period_name );
// die;
  // Create an array of URL parameters for easier maintenance.
  $data_params[0] = array();
  $data_params[0]['idSite'] = $matomo_site_id;
  $data_params[0]['date'] = $date;
  if( !isset( $form_state['values']['period_or_date'] ) ) {
    $data_params[0]['period'] = $period_name;
  } else {
    $data_params[0]['period'] = 'range';
  }
  $data_params[0]['disableLink'] = 1;
  $data_params[0]['module'] = 'Widgetize';
  $data_params[0]['action'] = 'iframe';
  $data_params[0]['disableLink'] = 1;
  $data_params[0]['widget'] = 1;

  if (!empty($matomo_auth)) {
    $data_params[0]['token_auth'] = $matomo_auth;
  }

  switch ($report) {

    case 'dashboard':
      $iframe_height[0] = 750;
      $title[0] = t('Matomo Dashboard');
      $data_params[0]['moduleToWidgetize'] = 'Dashboard';
      $data_params[0]['actionToWidgetize'] = 'index';
      $data_params[0]['period'] = 'week';
      $data_params[0]['date'] = date('Y-m-d');
      break;

    case 'all_websites_dashboard':
      $iframe_height[0] = 750;
      $title[0] = t('Dashboards');
      $data_params[0]['moduleToWidgetize'] = 'MultiSites';
      $data_params[0]['actionToWidgetize'] = 'standalone';
      break;

    case 'visitor_counter':
      $iframe_height[0] = 750;
      $title[0] = t('Real Time Visitor Count');
      $data_params[0]['moduleToWidgetize'] = 'Live';
      $data_params[0]['actionToWidgetize'] = 'getSimpleLastVisitCount';
      break;

    case 'visitor_profile':
      $iframe_height[0] = 750;
      $title[0] = t('Real Time Visitor Count');
      $data_params[0]['moduleToWidgetize'] = 'Live';
      $data_params[0]['actionToWidgetize'] = 'getVisitorProfilePopup';
      break;

    case 'visits_overview_with_graphs':
      $iframe_height[0] = 750;
      $title[0] = t('Visits Overview(with graph)');
      $data_params[0]['containerId'] = 'VisitOverviewWithGraph';
      $data_params[0]['moduleToWidgetize'] = 'CoreHome';
      $data_params[0]['actionToWidgetize'] = 'renderWidgetContainer';
      break;

    case 'visits_over_time':
        $iframe_height[0] = 750;
        $title[0] = t('Visits Over Time');
        $data_params[0]['moduleToWidgetize'] = 'VisitsSummary';
        $data_params[0]['actionToWidgetize'] = 'getEvolutionGraph';
        break;

    case 'visits_overview':
      $iframe_height[0] = 750;
      $title[0] = t('Visits Overview');
      $data_params[0]['moduleToWidgetize'] = 'VisitsSummary';
      $data_params[0]['actionToWidgetize'] = 'get';
      $data_params[0]['viewDataTable'] = 'sparklines';
      break;

    case 'visits_log':
      $iframe_height[0] = 750;
      $title[0] = t('Visits Log');
      $data_params[0]['moduleToWidgetize'] = 'Live';
      $data_params[0]['actionToWidgetize'] = 'getLastVisitsDetails';
      $data_params[0]['viewDataTable'] = 'VisitorLog';
      break;

    case 'visits_in_real_time':
      $iframe_height[0] = 750;
      $title[0] = t('Visits in Real-time');
      $data_params[0]['moduleToWidgetize'] = 'Live';
      $data_params[0]['actionToWidgetize'] = 'widget';
      break;

    case 'real_time_map':
      $iframe_height[0] = 750;
      $title[0] = t('Real-time Map');
      $data_params[0]['moduleToWidgetize'] = 'UserCountryMap';
      $data_params[0]['actionToWidgetize'] = 'realtimeMap';
      break;

    case 'user_ids':
      $iframe_height[0] = 750;
      $title[0] = t('User Ids');
      $data_params[0]['moduleToWidgetize'] = 'UserId';
      $data_params[0]['actionToWidgetize'] = 'getUsers';
      break;

    case 'custom_variables':
      $iframe_height[0] = 750;
      $title[0] = t('Custom Variables');
      $data_params[0]['moduleToWidgetize'] = 'CustomVariables';
      $data_params[0]['actionToWidgetize'] = 'getCustomVariables';
      break;

    case 'visitor_map':
      $iframe_height[0] = 750;
      $title[0] = t('Visitor Map');
      $data_params[0]['moduleToWidgetize'] = 'UserCountryMap';
      $data_params[0]['actionToWidgetize'] = 'visitorMap';
      break;

    case 'city':
      $iframe_height[0] = 750;
      $title[0] = t('City');
      $data_params[0]['moduleToWidgetize'] = 'UserCountry';
      $data_params[0]['actionToWidgetize'] = 'getCity';
      break;

    case 'continent':
      $iframe_height[0] = 750;
      $title[0] = t('Continent');
      $data_params[0]['moduleToWidgetize'] = 'UserCountry';
      $data_params[0]['actionToWidgetize'] = 'getContinent';
      break;

    case 'country':
      $iframe_height[0] = 750;
      $title[0] = t('Country');
      $data_params[0]['moduleToWidgetize'] = 'UserCountry';
      $data_params[0]['actionToWidgetize'] = 'getCountry';
      break;

    case 'region':
      $iframe_height[0] = 750;
      $title[0] = t('Region');
      $data_params[0]['moduleToWidgetize'] = 'UserCountry';
      $data_params[0]['actionToWidgetize'] = 'getRegion';
      break;

    case 'browser_language':
      $iframe_height[0] = 750;
      $title[0] = t('Region');
      $data_params[0]['moduleToWidgetize'] = 'UserLanguage';
      $data_params[0]['actionToWidgetize'] = 'getLanguage';
      break;

    case 'language_code':
      $iframe_height[0] = 750;
      $title[0] = t('Region');
      $data_params[0]['moduleToWidgetize'] = 'UserLanguage';
      $data_params[0]['actionToWidgetize'] = 'getLanguageCode';
      break;


    case 'devices_brand':
      $iframe_height[0] = 750;
      $title[0] = t('Devices Brand');
      $data_params[0]['moduleToWidgetize'] = 'DevicesDetection';
      $data_params[0]['actionToWidgetize'] = 'getBrand';
      break;

    case 'device_model':
      $iframe_height[0] = 750;
      $title[0] = t('Devices Model');
      $data_params[0]['moduleToWidgetize'] = 'DevicesDetection';
      $data_params[0]['actionToWidgetize'] = 'getModel';
      break;

    case 'device_type':
      $iframe_height[0] = 750;
      $title[0] = t('Devices Model');
      $data_params[0]['moduleToWidgetize'] = 'DevicesDetection';
      $data_params[0]['actionToWidgetize'] = 'getType';
      break;

    case 'screen_resolution':
      $iframe_height[0] = 750;
      $title[0] = t('Devices Model');
      $data_params[0]['moduleToWidgetize'] = 'Resolution';
      $data_params[0]['actionToWidgetize'] = 'getResolution';
      break;

    case 'browser_plugins':
      $iframe_height[0] = 750;
      $title[0] = t('Browser Plugins');
      $data_params[0]['moduleToWidgetize'] = 'DevicePlugins';
      $data_params[0]['actionToWidgetize'] = 'getPlugin';
      break;

    case 'browser_engines':
      $iframe_height[0] = 750;
      $title[0] = t('Browser engines');
      $data_params[0]['moduleToWidgetize'] = 'DevicesDetection';
      $data_params[0]['actionToWidgetize'] = 'getBrowsers';
      break;

    case 'browser_version':
      $iframe_height[0] = 750;
      $title[0] = t('Browser Version');
      $data_params[0]['moduleToWidgetize'] = 'DevicesDetection';
      $data_params[0]['actionToWidgetize'] = 'getBrowserVersions';
      break;

    case 'browsers':
      $iframe_height[0] = 750;
      $title[0] = t('Browser Version');
      $data_params[0]['moduleToWidgetize'] = 'DevicesDetection';
      $data_params[0]['actionToWidgetize'] = 'getBrowsers';
      break;

    case 'operating_system_families':
      $iframe_height[0] = 750;
      $title[0] = t('Operating System families');
      $data_params[0]['moduleToWidgetize'] = 'DevicesDetection';
      $data_params[0]['actionToWidgetize'] = 'getOsFamilies';
      break;

    case 'operating_system_versions':
      $iframe_height[0] = 750;
      $title[0] = t('Operating System versions');
      $data_params[0]['moduleToWidgetize'] = 'DevicesDetection';
      $data_params[0]['actionToWidgetize'] = 'getOsVersions';
      break;

    case 'configurations':
      $iframe_height[0] = 750;
      $title[0] = t('Operating System versions');
      $data_params[0]['moduleToWidgetize'] = 'Resolution';
      $data_params[0]['actionToWidgetize'] = 'getConfiguration';
      break;

    case 'visits_by_day_of_week':
      $iframe_height[0] = 750;
      $title[0] = t('Visits by Day of Week');
      $data_params[0]['moduleToWidgetize'] = 'VisitTime';
      $data_params[0]['actionToWidgetize'] = 'getByDayOfWeek';
      break;

    case 'visits_per_local_time':
      $iframe_height[0] = 750;
      $title[0] = t('Visits per local time');
      $data_params[0]['moduleToWidgetize'] = 'VisitTime';
      $data_params[0]['actionToWidgetize'] = 'getVisitInformationPerLocalTime';
      break;

    case 'visits_per_server_time':
      $iframe_height[0] = 750;
      $title[0] = t('Visits per local time');
      $data_params[0]['moduleToWidgetize'] = 'VisitTime';
      $data_params[0]['actionToWidgetize'] = 'getVisitInformationPerServerTime';
      break;

    case 'pages':
      $iframe_height[0] = 750;
      $title[0] = t('Pages');
      $data_params[0]['moduleToWidgetize'] = 'Actions';
      $data_params[0]['actionToWidgetize'] = 'getPageUrls';
      break;

    case 'entry_page_titles':
      $iframe_height[0] = 750;
      $title[0] = t('Entry Page Titles');
      $data_params[0]['moduleToWidgetize'] = 'Actions';
      $data_params[0]['actionToWidgetize'] = 'getEntryPageTitles';
      break;

    case 'entry_pages':
      $iframe_height[0] = 750;
      $title[0] = t('Entry Pages');
      $data_params[0]['moduleToWidgetize'] = 'Actions';
      $data_params[0]['actionToWidgetize'] = 'getEntryPageUrls';
      break;

    case 'exit_page_titles':
      $iframe_height[0] = 750;
      $title[0] = t('Exit Page Titles');
      $data_params[0]['moduleToWidgetize'] = 'Actions';
      $data_params[0]['actionToWidgetize'] = 'getExitPageTitles';
      break;

    case 'exit_pages':
      $iframe_height[0] = 750;
      $title[0] = t('Exit Pages');
      $data_params[0]['moduleToWidgetize'] = 'Actions';
      $data_params[0]['actionToWidgetize'] = 'getExitPageUrls';
      break;

    case 'page_titles':
      $iframe_height[0] = 750;
      $title[0] = t('Page Titles');
      $data_params[0]['moduleToWidgetize'] = 'Actions';
      $data_params[0]['actionToWidgetize'] = 'getPageTitles';
      break;

    case 'outlinks':
      $iframe_height[0] = 750;
      $title[0] = t('Outlinks');
      $data_params[0]['moduleToWidgetize'] = 'Actions';
      $data_params[0]['actionToWidgetize'] = 'getOutlinks';
      break;

    case 'downloads':
      $iframe_height[0] = 750;
      $title[0] = t('Outlinks');
      $data_params[0]['moduleToWidgetize'] = 'Actions';
      $data_params[0]['actionToWidgetize'] = 'getDownloads';
      break;

    case 'actions_content_name':
      $iframe_height[0] = 750;
      $title[0] = t('Outlinks');
      $data_params[0]['moduleToWidgetize'] = 'Contents';
      $data_params[0]['actionToWidgetize'] = 'getContentNames';
      break;

    case 'actions_content_piece':
      $iframe_height[0] = 750;
      $title[0] = t('Outlinks');
      $data_params[0]['moduleToWidgetize'] = 'Contents';
      $data_params[0]['actionToWidgetize'] = 'getContentPieces';
      break;

    case 'transitions':
      $iframe_height[0] = 750;
      $title[0] = t('Outlinks');
      $data_params[0]['moduleToWidgetize'] = 'Transitions';
      $data_params[0]['actionToWidgetize'] = 'getTransitions';
      break;

    case 'page_titles_site_search':
      $iframe_height[0] = 750;
      $title[0] = t('Page Titles Following a Site Search');
      $data_params[0]['moduleToWidgetize'] = 'Actions';
      $data_params[0]['actionToWidgetize'] = 'getPageTitlesFollowingSiteSearch';
      break;

    case 'page_following_site_search':
      $iframe_height[0] = 750;
      $title[0] = t('Pages Following a Site Search');
      $data_params[0]['moduleToWidgetize'] = 'Actions';
      $data_params[0]['actionToWidgetize'] = 'getPageUrlsFollowingSiteSearch';
      break;

    case 'search_categories':
      $iframe_height[0] = 750;
      $title[0] = t('Pages Following a Site Search');
      $data_params[0]['moduleToWidgetize'] = 'Actions';
      $data_params[0]['actionToWidgetize'] = 'getSiteSearchCategories';
      break;

    case 'site_search_keywords':
      $iframe_height[0] = 750;
      $title[0] = t('Pages Following a Site Search');
      $data_params[0]['moduleToWidgetize'] = 'Actions';
      $data_params[0]['actionToWidgetize'] = 'getSiteSearchKeywords';
      break;

    case 'search_keywords_with_no_results':
      $iframe_height[0] = 750;
      $title[0] = t('Search Keywords with No Results');
      $data_params[0]['moduleToWidgetize'] = 'Actions';
      $data_params[0]['actionToWidgetize'] = 'getSiteSearchNoResultKeywords';
      break;

    case 'event_actions':
      $iframe_height[0] = 750;
      $title[0] = t('Actions: Event Actions');
      $data_params[0]['moduleToWidgetize'] = 'Events';
      $data_params[0]['actionToWidgetize'] = 'getName';
      break;

    case 'event_categories':
      $iframe_height[0] = 750;
      $title[0] = t('Actions: Event Categories');
      $data_params[0]['moduleToWidgetize'] = 'Events';
      $data_params[0]['actionToWidgetize'] = 'getCategory';
      break;

    case 'event_names':
      $iframe_height[0] = 750;
      $title[0] = t('Actions: Event Names');
      $data_params[0]['moduleToWidgetize'] = 'Events';
      $data_params[0]['actionToWidgetize'] = 'getName';
      break;

    case 'returning_visits_over_time':
      $iframe_height[0] = 750;
      $title[0] = t('Returning Visits Over Time');
      $data_params[0]['moduleToWidgetize'] = 'VisitFrequency';
      $data_params[0]['actionToWidgetize'] = 'getEvolutionGraph';
      break;

    case 'frequency_overview':
      $iframe_height[0] = 750;
      $title[0] = t('Frequency Overview');
      $data_params[0]['moduleToWidgetize'] = 'VisitFrequency';
      $data_params[0]['actionToWidgetize'] = 'get';
      break;

    case 'visits_by_days_visit':
      $iframe_height[0] = 750;
      $title[0] = t('Visits by Days Since Last Visit');
      $data_params[0]['moduleToWidgetize'] = 'VisitorInterest';
      $data_params[0]['actionToWidgetize'] = 'getNumberOfVisitsByDaysSinceLast';
      break;

    case 'visits_by_visit_number':
      $iframe_height[0] = 750;
      $title[0] = t('Visits by Visit Number');
      $data_params[0]['moduleToWidgetize'] = 'VisitorInterest';
      $data_params[0]['actionToWidgetize'] = 'getNumberOfVisitsByVisitCount';
      break;

    case 'visits_per_no_of_pages':
      $iframe_height[0] = 750;
      $title[0] = t('Visits by Visit Number');
      $data_params[0]['moduleToWidgetize'] = 'VisitorInterest';
      $data_params[0]['actionToWidgetize'] = 'getNumberOfVisitsPerPage';
      break;

    case 'visits_per_visit_duration':
      $iframe_height[0] = 750;
      $title[0] = t('Visits per visit duration');
      $data_params[0]['moduleToWidgetize'] = 'VisitorInterest';
      $data_params[0]['actionToWidgetize'] = 'getNumberOfVisitsPerVisitDuration';
      break;

    case 'referrers':
      $iframe_height[0] = 750;
      $title[0] = t('Referrers');
      $data_params[0]['moduleToWidgetize'] = 'Referrers';
      $data_params[0]['actionToWidgetize'] = 'getAll';
      break;

    case 'channel_types':
      $iframe_height[0] = 750;
      $title[0] = t('Channel Types');
      $data_params[0]['moduleToWidgetize'] = 'Referrers';
      $data_params[0]['actionToWidgetize'] = 'getReferrerType';
      break;

    case 'keywords':
      $iframe_height[0] = 750;
      $title[0] = t('Keywords');
      $data_params[0]['moduleToWidgetize'] = 'Referrers';
      $data_params[0]['actionToWidgetize'] = 'getKeywords';
      break;

    case 'search_engines':
      $iframe_height[0] = 750;
      $title[0] = t('Search Engines');
      $data_params[0]['moduleToWidgetize'] = 'Referrers';
      $data_params[0]['actionToWidgetize'] = 'getSearchEngines';
      break;

    case 'websites':
      $iframe_height[0] = 750;
      $title[0] = t('Websites');
      $data_params[0]['moduleToWidgetize'] = 'Referrers';
      $data_params[0]['actionToWidgetize'] = 'getWebsites';
      break;

    case 'social_networks':
      $iframe_height[0] = 750;
      $title[0] = t('Social Networks');
      $data_params[0]['moduleToWidgetize'] = 'Referrers';
      $data_params[0]['actionToWidgetize'] = 'getSocials';
      break;

    case 'campaigns':
      $iframe_height[0] = 750;
      $title[0] = t('Campaigns');
      $data_params[0]['moduleToWidgetize'] = 'Referrers';
      $data_params[0]['actionToWidgetize'] = 'getCampaigns';
      break;

    case 'campaign_url_builder':
      $iframe_height[0] = 750;
      $title[0] = t('Campaign url builder');
      $data_params[0]['moduleToWidgetize'] = 'Referrers';
      $data_params[0]['actionToWidgetize'] = 'getCampaignUrlBuilder';
      break;

    case 'overview':
      $iframe_height[0] = 750;
      $title[0] = t('Goals');
      $data_params[0]['moduleToWidgetize'] = 'CoreHome';
      $data_params[0]['actionToWidgetize'] = 'renderWidgetContainer';
      $data_params[0]['containerId'] = 'GoalsOverview';
      break;

    case 'seo_rankings':
      $iframe_height[0] = 750;
      $title[0] = t('SEO Rankings');
      $data_params[0]['moduleToWidgetize'] = 'SEO';
      $data_params[0]['actionToWidgetize'] = 'getRank';
      break;

  }
  // Build the data URL with all params and urlencode it.
  foreach ($data_params as $key => $data) {
    $theme_args['data_url'][] = array(
      'url' => $matomo_url . 'index.php?' . drupal_http_build_query($data),
      'title' => $title[$key],
      'iframe_height' => (isset($iframe_height[$key]) && $iframe_height[$key] > 0 ? $iframe_height[$key] : 400),
      'empty_text' => (isset($empty_text) ? $empty_text : NULL),
    );
  }

  // Render HTML code.
  $output = theme('matomo_reports', $theme_args);
  $form['content'] = array(
    '#type' => 'item',
    '#markup' => $output,
  );
  return $form;
}

/**
 * Reports form submit function.
 */
function matomo_reports_reports_submit($form, &$form_state) {
  $_SESSION['matomo_reports_start_date'] = $form_state['values']['matomo-start-date'];
  $_SESSION['matomo_reports_end_date'] = $form_state['values']['matomo-end-date'];
  $_SESSION['matomo_reports_site'] = $form_state['values']['site'];
  $_SESSION['matomo_reports_period'] = $form_state['values']['period'];
  $_SESSION['matomo_reports_period_or_date'] = $form_state['values']['period_or_date'];
}

function _ajax_matomo_report_end_date_callback($form, $form_state) {

  return $form['matomo_filters']['matomo-end-date'];

}

/**
 * Helper function to return the starting and ending dates according to the
 * selected period.
 *
 * @param int $period
 *   Selected period.
 *
 * @return string
 *   Formatted date.
 */
function _matomo_reports_select_period($period) {
  switch ($period) {
    case 0:
      $date = date("Y-m-d");
      break;

    case 1:
      $d = mktime(0, 0, 0, date("m"), date("d") - 1, date("Y"));
      $date = date("Y-m-d", $d);
      break;

    case 2:
      $d = mktime(0, 0, 0, date("m"), date("d") - 7, date("Y"));
      $date = date("Y-m-d", $d);
      break;

    case 3:
      $d = mktime(0, 0, 0, date("m") - 1, date("d"), date("Y"));
      $date = date("Y-m-d", $d);
      break;

    case 4:
      $d = mktime(0, 0, 0, date("m"), date("d"), date("Y") - 1);
      $date = date("Y-m-d", $d);
      break;
  }
  return $date;
}

/**
 * Helper function to return the name of the selected period.
 *
 * @param int $period
 *   Selected period.
 *
 * @return string
 *   Name of period.
 */
function _matomo_reports_get_period_name($period) {
  // Possible periods are day, week, month, year.
  switch ($period) {
    case 0:
      $p = "day";
      break;

    case 1:
      $p = "day";
      break;

    case 2:
      $p = "week";
      break;

    case 3:
      $p = "month";
      break;

    case 4:
      $p = "year";
      break;
  }
  return $p;
}