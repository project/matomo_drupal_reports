<?php

function matomo_drupal_customize_reports($form, &$form_state) {

  $matomo_reports_default_values = unserialize( variable_get( 'matomo_reports_variable', '' ) );

  $form['#attached']['js'][] = drupal_get_path('module', 'matomo_drupal_reports') . '/js/matomo_drupal_reports.js';

  $reports_options = array(
    'visitors' => array(
      'visitors-counter' => 'Real Time Visitor Count',
      'visitor-profile' => 'Visitor profile',
      'visits-overview-with-graphs' => 'Visits Overview(G)',
      'visits-over-time' => 'Visits Over Time',
      'visits-overview' => 'Visits Overview',
      'visits-log' => 'Visits Log',
      'visits-in-real-time' => 'Visits in Real-time',
      'real-time-map' => 'Real-time Map',
      'user-ids' => 'User IDs',
      'custom-variables' => 'Custom Variables',
    ),
    'visitors-locations' => array(
      'visitor-map' => 'Visitor Map',
      'city' => 'City',
      'continent' => 'Continent',
      'country' => 'Country',
      'region' => 'Region',
      'browser-language' => 'Browser language',
      'language-code' => 'Language code',
    ),
    'visitors-devices' => array(
      'devices-brand' => 'Devices Brand',
      'device-model' => 'Device model',
      'device-type' => 'Device type',
      'screen-resolution' => 'Screen Resolution',
    ),
    'visitors-software' => array(
      'browser-plugins' => 'Browser Plugins',
      'browser-engines' => 'Browser engines',
      'browser-version' => 'Browser version',
      'browsers' => 'Browsers',
      'operating-system-families' => 'Operating System families',
      'operating-system-versions' => 'Operating System versions',
      'configurations' => 'Configurations',
    ),
    'visitors-times' => array(
      'visits-by-day-of-week' => 'Visits by Day of Week',
      'visits-per-local-time' => 'Visits per local time',
      'visits-per-server-time' => 'Visits per server time',
    ),
    'behaviour' => array(
      'pages' => 'Pages',
      'entry-page-titles' => 'Entry Page Titles',
      'entry-pages' => 'Entry pages',
      'exit-page-titles' => 'Exit page titles',
      'exit-pages' => 'Exit pages',
      'page-titles' => 'Page titles',
      'outlinks' => 'Outlinks',
      'downloads' => 'Downloads',
      'actions-content-name' => 'Actions: Content Name',
      'actions-content-piece' => 'Actions: Content Piece',
      'transitions' => 'Transitions',
    ),
    'behaviour-site-search' => array(
      'page-titles-site-search' => 'Page Titles Following a Site Search',
      'pages-following-site-search' => 'Pages Following a Site Search',
      'search-categories' => 'Search Categories',
      'site-search-keywords' => 'Site Search Keywords',
      'search-keywords-with-no-results' => 'Search Keywords with No Results',
    ),
    'behaviour-events' => array(
      'event-actions' => 'Actions: Event Actions',
      'event-categories' => 'Actions: Event Categories',
      'event-names' => 'Actions: Event Names',
    ),
    'behaviour-engagement' => array(
      'returning-visits-over-time' => 'Returning Visits Over Time',
      'frequency-overview' => 'Frequency Overview',
      'visits-by-days-visit' => 'Visits by Days Since Last Visit',
      'visits-by-visit-number' => 'Visits by Visit Number',
      'visits-per-no-of-pages' => 'Visits per number of pages',
      'visits-per-visit-duration' => 'Visits per visit duration',
    ),
    'acquisition' => array(
      'referrers' => 'Referrers',
      'channel-types' => 'Channel Types',
      'keywords' => 'Keywords',
      'search-engines' => 'Search Engines',
      'websites' => 'Websites',
      'social-networks' => 'Social Networks',
      'campaigns' => 'Campaigns',
      'campaign-url-builder' => 'Campaign URL Builder',
    ),
    'goals' => array(
        'overview' => 'Overview',
    ),
    'seo' => array(
        'seo_rankings' => 'SEO Rankings',
    ),
  );

  $i = 0;
  foreach($reports_options as $key => $r_opt ) {
    $chk_form = [];

//     $opt_form = [
//         '#type' => 'radio',
//         '#title' => t(ucfirst($key)),
//         '#name' => 'matomo_reports',
//     ];
//     $bool_main_tab = '';
//     if( $i == 0 && empty($matomo_reports_default_values) ) {
//       $bool_main_tab = checked;
//     }

    $i++;
//     $opt_form['#attributes'] = array('value' => $key . '--main-tab-deafult', $bool_main_tab => $bool_main_tab, 'parent-link' => $key);


    $chk_form = array(
        '#type' => 'checkbox',
        '#title' => t(ucfirst($key)),
    );
    if( isset( $matomo_reports_default_values[$key . '--main-tab'] ) && $matomo_reports_default_values[$key . '--main-tab'] == 1 ) {
      $chk_form['#attributes'] = array('checked' => 'checked','parent' => $key);
    } else if( empty($matomo_reports_default_values) ) {
      $chk_form['#attributes'] = array('checked' => 'checked','parent' => $key);
    } else {
      $chk_form['#attributes'] = array('parent' => $key);
    }

    $form[$key]['data'][$key . '--main-tab'] = $chk_form;
//     $form[$key]['data'][$key . '--main-tab-deafult'] = $opt_form;
    $form[$key]['data'][$key . '--sub-tab'] = [];
    $form[$key]['data'][$key . '--sub-tab-deafult'] = [];

    $j = 0;
    foreach( $r_opt as $inner_key => $inner_r_opt ) {
      $opt_form = [];
      $chk_form = [];
      $opt_form = [
          '#type' => 'radio',
          '#title' => t($inner_r_opt),
          '#name' => $key,
          '#attributes' => array('parent' => $key),
      ];
      $bool_sub_tab = '';
      if( $j == 0 && empty($matomo_reports_default_values) ) {
        $bool_sub_tab = checked;
      } else if( isset( $matomo_reports_default_values[$inner_key . '--sub-tab-deafult'] ) && $matomo_reports_default_values[$inner_key . '--sub-tab-deafult'] == 1 ) {
        $bool_sub_tab = checked;
      }
      $j++;
      $opt_form['#attributes'] = array('value' => $inner_key . '--sub-tab-deafult', $bool_sub_tab => $bool_sub_tab, 'parent-link' => $key);
      $chk_form = array(
          '#type' => 'checkbox',
          '#title' => t($inner_r_opt),
      );
      if( isset( $matomo_reports_default_values[$inner_key . '--sub-tab'] ) && $matomo_reports_default_values[$inner_key . '--sub-tab'] == 1 ) {
        $chk_form['#attributes'] = array('checked' => 'checked', 'parent-link' => $key);
      } else if( empty($matomo_reports_default_values) ) {
        $chk_form['#attributes'] = array('checked' => 'checked', 'parent-link' => $key);
      } else {
        $chk_form['#attributes'] = array('parent-link' => $key);
      }

      $form[$inner_key]['data'][$inner_key . '--sub-tab-deafult'] = $opt_form;
      $form[$inner_key]['data'][$inner_key . '--sub-tab'] = $chk_form;
      $form[$inner_key]['data'][$inner_key . '--main-tab'] = [];
    }
  }

  $form['matomo_save']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#weight' => 15,
  );

  return $form;

}

function theme_matomo_drupal_customize_reports($variables) {

  $form = $variables['form'];

  $header = array(
      'main-tab' => t('Main Tab(s)'),
      'sub-tab' => t('Sub Tab(s)'),
      'sub-tab-deafult' => t('Sub Tab(s) Default *'),
  );

  $options = [];
  foreach( $form as $key => $arr_form ) {
    if( is_array( $form[$key] ) ) {
      foreach( $arr_form as $inner_key => $arr_inner_form ) {
        if (strpos($key, '#') !== false) {
          continue;
        }
        if( $inner_key == 'data' ) {
          $options[$key]['data']['main-tab'] = drupal_render( $form[$key]['data'][$key . '--main-tab'] );
          $options[$key]['data']['sub-tab'] = drupal_render( $form[$key]['data'][$key . '--sub-tab'] );
          $options[$key]['data']['sub-tab-deafult'] = drupal_render( $form[$key]['data'][$key . '--sub-tab-deafult'] );
        }
      }
    }
  }

  $output = theme('table', array(
      'header' => $header,
      'rows' => $options,
  ));

  $output .= drupal_render_children( $form );

  return $output;
}

/**
 * Reports form submit function.
 */
function matomo_drupal_customize_reports_submit($form, &$form_state) {

  $arr_values = $form_state['values'];
  foreach( $form_state['values'] as $key => $val ) {
    if (strpos($key, '--') !== false) {
      foreach( $_POST as $p_val ) {
        if( $key = $p_val ) {
          $arr_values[$key] = 1;
        }
      }
    }
  }
//   if( isset( $_POST['matomo_reports'] ) ) {
//     $arr_values[$_POST['matomo_reports']] = 1;
//   }
  unset( $arr_values['submit'] );
  unset( $arr_values['form_build_id'] );
  unset( $arr_values['form_token'] );
  unset( $arr_values['form_id'] );
  unset( $arr_values['op'] );

  variable_set( 'matomo_reports_variable', serialize($arr_values) );

  variable_set('menu_rebuild_needed', TRUE);

}