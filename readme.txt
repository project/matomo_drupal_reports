INTRODUCTION
------------

The Matomo Drupal Reports module provides us the reports under 
reports menu( admin/reports/matomo_reports ).

Matomo, formerly known as Piwik, is a downloadable, Free (GPL licensed) web analytics software platform. 
It provides detailed reports on your website and its visitors, including the search engines and keywords they used,
 the language they speak, which pages they like, the files they download and so much more.
Matomo (Piwik) aims to be an open source alternative to Google Analytics.
Matomo is PHP MySQL software which you download and install on your own webserver.

It tracks online visits to one or more websites and displays reports on these visits for analysis.

HOW TO USE
-----------

This modules can be used to show reports from the Matomo.

There are two main APIs in Matomo (Piwik)
	-Analytics Web API: used to request all Matomo (Piwik) reports and to manage (add, update, delete) websites, users, permissions, email reports, etc.
	-Tracking Web API: used to record data in Matomo (Piwik) using the JavaScript Code, or using the other languages
	
You can use the Analytics API to programmatically request reports (number of visits, page URLs, page titles, 
user settings, search engines, keywords, referrer websites, user browsers, etc.) for a list of websites, and a given date and period (day, week, month, year).
The data can be requested in any format: xml, csv, json, Excel, etc.
When you’re using Matomo (Piwik) you can export the data of any report by clicking the “Export” icon below each report.

REQUIREMENTS
------------

This module requires the following modules:

 * Date and Date popup (https://drupal.org/project/date)

INSTALLATION
------------
 
 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.

CONFIGURATION
-------------

There is configuration at (admin/config/administration/matomo_drupal_reports && admin/config/administration/customize-reports) 